using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyBalance : MonoBehaviour
{
    [SerializeField] private Player _player;
    
    [SerializeField] private TMP_Text _text;
    
    private void Awake()
    {
        _player.ChangeMoney += DisplayMoney;
    }
    
    private void OnDisable()
    {
        _player.ChangeMoney -= DisplayMoney;
    }
    
    public void DisplayMoney(float amount)
    {
        _text.text = amount.ToString();
    }
}
