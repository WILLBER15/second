using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Bar : MonoBehaviour
{
    [SerializeField] protected Player _player;
    [SerializeField] protected Slider _slider;

    protected void Display(float value, float maxValue)
    {
        _slider.value = value/10;
        _slider.maxValue = maxValue / 10;
    }
}
