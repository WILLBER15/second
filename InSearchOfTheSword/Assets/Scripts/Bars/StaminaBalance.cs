using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaBalance : Bar
{
    private void Awake()
    {
        _player.ChangeStamina += Display;
        
        _slider.maxValue = _player.MaxStamina/10;
    }
    
    private void OnDisable()
    {
        _player.ChangeStamina -= Display;
    }
}
