using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : Bar
{
    private void Awake()
    {
        _player.ChangeHealth += Display;
        
        _slider.maxValue = _player.MaxHealth/10;
    }
    
    private void OnDisable()
    {
        _player.ChangeHealth -= Display;
    }
}
