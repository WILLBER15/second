using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelViwer : MonoBehaviour
{
    [SerializeField] private PlayerLevels _level;
    
    [SerializeField] private TMP_Text _text;
    [SerializeField] private Slider _slider;

    private void Awake()
    {
        _level.LevelUped += Viewe;
    }

    private void Viewe(int level, float currentExperiance, float maxExperiance)
    {
        _text.text ="Level " + level.ToString();
        
        _slider.maxValue = maxExperiance / 100;
        _slider.value = currentExperiance / 100;
    }
}
