using UnityEngine;
using UnityEngine.SceneManagement;

public class TravelToNextLocation : MonoBehaviour
{
    [SerializeField] private string _nameNextLocation;

    [SerializeField] private float _timer;

    [SerializeField] private Menu _menu;

    public GameObject creare;
    public GameObject menu;
    public GameObject seting;

    private float _deley;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Player player))
        {
            StartTimer();
        }
    }

    private void Update()
    {
        if (_deley != 0)
        {
            _deley -= Time.deltaTime;

            if (_deley <= 0)
                GoToNextLocation();
        }
    }
    
     //меню глвное

    public void openCreate()
    {
        creare.SetActive (true);
        menu.SetActive(false);
        seting.SetActive(false);
    }

    public void openMenu()
    {
        creare.SetActive(false);
        menu.SetActive(true);
        seting.SetActive(false);
    }
    public void openSeting()
    {
        creare.SetActive(false);
        menu.SetActive(false);
        seting.SetActive(true);
    }

    public void StartTimer()
    {
        _deley = _timer;
    }

    private void GoToNextLocation()
    {
        _menu.LoadScene(_nameNextLocation);
    }
    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(name);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
