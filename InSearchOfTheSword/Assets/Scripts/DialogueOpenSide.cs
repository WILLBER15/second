using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOpenSide : MonoBehaviour
{
    [SerializeField] private DialogueViewer _dialogue;

    [SerializeField] private Mode _mode;

    private PlayerInput _input;

    private void Awake()
    {
        _input = new PlayerInput();
        _input.UIOpen.Open.performed += ctx => DialogueOpen();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Player player))
        {
            if (_mode == Mode.ButtonMode)
            {
                _input.Enable();
            }
            else
            {
                DialogueOpen();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out Player player))
        {
            _input.Disable();
        }
    }
    
    private void DialogueOpen()
    {
        _dialogue.gameObject.SetActive(true);
        _dialogue.VieweCurrentPhase();
    }
    
    private enum Mode
    {
        TriggerMode, 
        ButtonMode,
    }
}
