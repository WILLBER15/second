using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperianceBonus : Bonus
{
    [SerializeField] private float _speed;
    
    private PlayerLevels _playerLevels;

    private float _experianceLevel;


    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, _playerLevels.transform.position, Time.deltaTime * _speed);
    }

    public void Init(float experiance, PlayerLevels playerLevels)
    {
        _experianceLevel = experiance;
        _playerLevels = playerLevels;
    }

    protected override void AddBonus()
    {
        _playerLevels.AddExperiance(_experianceLevel);
    }
}
