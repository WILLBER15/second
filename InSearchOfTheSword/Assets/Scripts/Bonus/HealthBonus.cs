using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBonus : Bonus
{
    [SerializeField] private float _treatmentAmount;
    
    protected override void AddBonus()
    {
        _player.AddHealth(_treatmentAmount);
    }
}
