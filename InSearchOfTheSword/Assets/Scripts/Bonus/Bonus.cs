using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public abstract class Bonus : MonoBehaviour
{ 
    protected Player _player;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.TryGetComponent(out Player player) )
        {
            _player = player;
            AddBonus();

            Destroy(gameObject);
        }
    }

    protected abstract void AddBonus();
}
