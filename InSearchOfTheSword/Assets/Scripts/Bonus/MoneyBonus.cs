using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyBonus : Bonus
{
    [SerializeField] private float _reward;
    
    protected override void AddBonus()
    {
        _player.AddMoney(_reward);
    }
}
