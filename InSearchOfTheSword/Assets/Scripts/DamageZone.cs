using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{
   [SerializeField] private float _damage;
   
   private Player _player;

   private void OnTriggerEnter2D(Collider2D other)
   {
      if (other.TryGetComponent(out Player player))
      {
         _player = player;

         StartCoroutine(Damage());
      }
   }

   private void OnTriggerExit2D(Collider2D other)
   {
      if (other.TryGetComponent(out Player player))
      {
         StopCoroutine(Damage());
         _player = null;
      }
   }

   private IEnumerator Damage()
   {
      var seconds = new WaitForSeconds(1);
      
      while (_player != null)
      {
         _player.TakeDamage(_damage);

         yield return seconds;
         
         if (_player == null)
         {
            break;
         }
      }
   }
}
