// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input/PlayerInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInput"",
    ""maps"": [
        {
            ""name"": ""Movement"",
            ""id"": ""bcdbf8a3-c825-42d0-ba02-1590f64f3a06"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""4d57a7b6-7b4d-4e85-86e3-2da5cea51abf"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""12463e8a-65a7-4dbc-84d2-171e1868ed5f"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""b1dea583-4938-4b14-a2c9-067fba6c6810"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""042c1354-b15e-474a-84bc-a72e194fa549"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""1233a628-98f0-431f-afd9-f85442ad1e79"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""1e370c2e-07db-4064-8003-81163a610edf"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""Attack"",
            ""id"": ""8f6a7d3a-8f7a-42b7-a008-431496423e06"",
            ""actions"": [
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""4d98385c-1e41-48e5-8dd8-6f21c45d31a7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""TimerInterection(Duration=0.3)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""25d9a9dc-fd12-4343-8408-27a4fa02b726"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Dash"",
            ""id"": ""0737696b-b5f9-4eb2-9c4d-25b1b8a8b87b"",
            ""actions"": [
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""f3a4f223-4108-4a57-b47c-42c58ed26763"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""TimerInterection(Duration=1)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""e105943a-dce5-40aa-8f2e-448fb5323be6"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UIOpen"",
            ""id"": ""8dc2a4c1-e86d-4914-ae3e-3f995f715ce8"",
            ""actions"": [
                {
                    ""name"": ""Open"",
                    ""type"": ""Button"",
                    ""id"": ""954b0ccd-a3fc-423b-b06a-e25d93b1ba5c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c4ea50e5-9a5c-4e1c-a58c-dc269d999356"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Open"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menu"",
            ""id"": ""4c7862e1-8315-4606-a54f-4810908d1892"",
            ""actions"": [
                {
                    ""name"": ""Exit"",
                    ""type"": ""Button"",
                    ""id"": ""56c9cbd8-375f-4dfd-9fe6-022e364c16d0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""615e4e13-ca3d-41ab-ac6f-345c92425f74"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""QuestLog"",
            ""id"": ""0a19053b-56ce-455f-bd2a-4804a8697ca9"",
            ""actions"": [
                {
                    ""name"": ""OpenClose"",
                    ""type"": ""Button"",
                    ""id"": ""53549c43-7249-4bdb-a29f-d6367ec944bb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6c5f1ccc-8b90-468d-b2b6-bcac01d51cbd"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""OpenClose"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Movement
        m_Movement = asset.FindActionMap("Movement", throwIfNotFound: true);
        m_Movement_Move = m_Movement.FindAction("Move", throwIfNotFound: true);
        // Attack
        m_Attack = asset.FindActionMap("Attack", throwIfNotFound: true);
        m_Attack_Attack = m_Attack.FindAction("Attack", throwIfNotFound: true);
        // Dash
        m_Dash = asset.FindActionMap("Dash", throwIfNotFound: true);
        m_Dash_Dash = m_Dash.FindAction("Dash", throwIfNotFound: true);
        // UIOpen
        m_UIOpen = asset.FindActionMap("UIOpen", throwIfNotFound: true);
        m_UIOpen_Open = m_UIOpen.FindAction("Open", throwIfNotFound: true);
        // Menu
        m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
        m_Menu_Exit = m_Menu.FindAction("Exit", throwIfNotFound: true);
        // QuestLog
        m_QuestLog = asset.FindActionMap("QuestLog", throwIfNotFound: true);
        m_QuestLog_OpenClose = m_QuestLog.FindAction("OpenClose", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Movement
    private readonly InputActionMap m_Movement;
    private IMovementActions m_MovementActionsCallbackInterface;
    private readonly InputAction m_Movement_Move;
    public struct MovementActions
    {
        private @PlayerInput m_Wrapper;
        public MovementActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Movement_Move;
        public InputActionMap Get() { return m_Wrapper.m_Movement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MovementActions set) { return set.Get(); }
        public void SetCallbacks(IMovementActions instance)
        {
            if (m_Wrapper.m_MovementActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMove;
            }
            m_Wrapper.m_MovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
            }
        }
    }
    public MovementActions @Movement => new MovementActions(this);

    // Attack
    private readonly InputActionMap m_Attack;
    private IAttackActions m_AttackActionsCallbackInterface;
    private readonly InputAction m_Attack_Attack;
    public struct AttackActions
    {
        private @PlayerInput m_Wrapper;
        public AttackActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Attack => m_Wrapper.m_Attack_Attack;
        public InputActionMap Get() { return m_Wrapper.m_Attack; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(AttackActions set) { return set.Get(); }
        public void SetCallbacks(IAttackActions instance)
        {
            if (m_Wrapper.m_AttackActionsCallbackInterface != null)
            {
                @Attack.started -= m_Wrapper.m_AttackActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_AttackActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_AttackActionsCallbackInterface.OnAttack;
            }
            m_Wrapper.m_AttackActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
            }
        }
    }
    public AttackActions @Attack => new AttackActions(this);

    // Dash
    private readonly InputActionMap m_Dash;
    private IDashActions m_DashActionsCallbackInterface;
    private readonly InputAction m_Dash_Dash;
    public struct DashActions
    {
        private @PlayerInput m_Wrapper;
        public DashActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Dash => m_Wrapper.m_Dash_Dash;
        public InputActionMap Get() { return m_Wrapper.m_Dash; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DashActions set) { return set.Get(); }
        public void SetCallbacks(IDashActions instance)
        {
            if (m_Wrapper.m_DashActionsCallbackInterface != null)
            {
                @Dash.started -= m_Wrapper.m_DashActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_DashActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_DashActionsCallbackInterface.OnDash;
            }
            m_Wrapper.m_DashActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
            }
        }
    }
    public DashActions @Dash => new DashActions(this);

    // UIOpen
    private readonly InputActionMap m_UIOpen;
    private IUIOpenActions m_UIOpenActionsCallbackInterface;
    private readonly InputAction m_UIOpen_Open;
    public struct UIOpenActions
    {
        private @PlayerInput m_Wrapper;
        public UIOpenActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Open => m_Wrapper.m_UIOpen_Open;
        public InputActionMap Get() { return m_Wrapper.m_UIOpen; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIOpenActions set) { return set.Get(); }
        public void SetCallbacks(IUIOpenActions instance)
        {
            if (m_Wrapper.m_UIOpenActionsCallbackInterface != null)
            {
                @Open.started -= m_Wrapper.m_UIOpenActionsCallbackInterface.OnOpen;
                @Open.performed -= m_Wrapper.m_UIOpenActionsCallbackInterface.OnOpen;
                @Open.canceled -= m_Wrapper.m_UIOpenActionsCallbackInterface.OnOpen;
            }
            m_Wrapper.m_UIOpenActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Open.started += instance.OnOpen;
                @Open.performed += instance.OnOpen;
                @Open.canceled += instance.OnOpen;
            }
        }
    }
    public UIOpenActions @UIOpen => new UIOpenActions(this);

    // Menu
    private readonly InputActionMap m_Menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private readonly InputAction m_Menu_Exit;
    public struct MenuActions
    {
        private @PlayerInput m_Wrapper;
        public MenuActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Exit => m_Wrapper.m_Menu_Exit;
        public InputActionMap Get() { return m_Wrapper.m_Menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                @Exit.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnExit;
                @Exit.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnExit;
                @Exit.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnExit;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Exit.started += instance.OnExit;
                @Exit.performed += instance.OnExit;
                @Exit.canceled += instance.OnExit;
            }
        }
    }
    public MenuActions @Menu => new MenuActions(this);

    // QuestLog
    private readonly InputActionMap m_QuestLog;
    private IQuestLogActions m_QuestLogActionsCallbackInterface;
    private readonly InputAction m_QuestLog_OpenClose;
    public struct QuestLogActions
    {
        private @PlayerInput m_Wrapper;
        public QuestLogActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @OpenClose => m_Wrapper.m_QuestLog_OpenClose;
        public InputActionMap Get() { return m_Wrapper.m_QuestLog; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(QuestLogActions set) { return set.Get(); }
        public void SetCallbacks(IQuestLogActions instance)
        {
            if (m_Wrapper.m_QuestLogActionsCallbackInterface != null)
            {
                @OpenClose.started -= m_Wrapper.m_QuestLogActionsCallbackInterface.OnOpenClose;
                @OpenClose.performed -= m_Wrapper.m_QuestLogActionsCallbackInterface.OnOpenClose;
                @OpenClose.canceled -= m_Wrapper.m_QuestLogActionsCallbackInterface.OnOpenClose;
            }
            m_Wrapper.m_QuestLogActionsCallbackInterface = instance;
            if (instance != null)
            {
                @OpenClose.started += instance.OnOpenClose;
                @OpenClose.performed += instance.OnOpenClose;
                @OpenClose.canceled += instance.OnOpenClose;
            }
        }
    }
    public QuestLogActions @QuestLog => new QuestLogActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    public interface IMovementActions
    {
        void OnMove(InputAction.CallbackContext context);
    }
    public interface IAttackActions
    {
        void OnAttack(InputAction.CallbackContext context);
    }
    public interface IDashActions
    {
        void OnDash(InputAction.CallbackContext context);
    }
    public interface IUIOpenActions
    {
        void OnOpen(InputAction.CallbackContext context);
    }
    public interface IMenuActions
    {
        void OnExit(InputAction.CallbackContext context);
    }
    public interface IQuestLogActions
    {
        void OnOpenClose(InputAction.CallbackContext context);
    }
}
