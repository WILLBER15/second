using UnityEngine.InputSystem;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
public class TimerInterection : IInputInteraction
{
    public float Duration;

    private bool _onClick;
    
     static TimerInterection()
    {
        InputSystem.RegisterInteraction<TimerInterection>();
    }

     [RuntimeInitializeOnLoadMethod]
     private static void Register(){}

     public void Process(ref InputInteractionContext context)
    {
        InputSystem.RegisterInteraction<TimerInterection>();
        
        if (context.timerHasExpired)
        {
            context.Performed();
            _onClick = false;
            return;
        }

        if(context.phase == InputActionPhase.Waiting && context.ControlIsActuated(0.5f))
        {
            context.Started();
        }
        
        if(context.phase == InputActionPhase.Started && context.ControlIsActuated(0.5f))
        {
            if (_onClick == false)
            {
                context.SetTimeout(Duration);

                _onClick = true;
            }
        }

    }


    public void Reset()
    {
    }
}
