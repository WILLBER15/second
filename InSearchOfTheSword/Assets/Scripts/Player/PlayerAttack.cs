using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private PlayerLevels _playerLevels;
    
    [SerializeField] private float _damage;
    [SerializeField] private float _throwDistance;

     private Enemy.Enemy _enemy;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.TryGetComponent(out Enemy.Enemy enemy))
        {
            _enemy = enemy;
        }
    }

    public void Attack()
    {
        if (_enemy == null)
            return;

        var position = (_enemy.transform.position - _playerLevels.gameObject.transform.position) * _throwDistance;
        _enemy.transform.Translate(position);

        _enemy.TakeDamage(_damage, _playerLevels);

        _enemy = null;
    }

    public void AddDamage(float damage)
    {
        _damage += damage;
    }
}
