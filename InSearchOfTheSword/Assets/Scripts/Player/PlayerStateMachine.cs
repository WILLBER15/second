using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement), typeof(Player), typeof(Animator))]
public class PlayerStateMachine : MonoBehaviour
{
    [SerializeField] private PlayerAttack _attack;

    [SerializeField] private float _dashStamine;
    [SerializeField] private float _attackStamine;
    
    private Animator _animator;
    
    private PlayerMovement _movement;
    private Player _player;

    private PlayerInput _input;
    private Vector2 _direction;
    
    private State _currentState;

    private void Awake()
    {
        _input = new PlayerInput();
        _input.Enable();

        _input.Attack.Attack.started += ctx => Attack();
        _input.Attack.Attack.performed += ctx => Idle();
        _input.Dash.Dash.started += ctx => Dash();
        _input.Dash.Dash.performed += ctx => Idle();
        
        _movement = gameObject.GetComponent<PlayerMovement>();
        _player = gameObject.GetComponent<Player>();
        _animator = gameObject.GetComponent<Animator>();
    }

    private void Update()
    {
        _direction = _input.Movement.Move.ReadValue<Vector2>();
        Move(_direction);
        
        _animator.SetFloat("MoveDirectionX", _direction.x);
        _animator.SetFloat("MoveDirectionY", _direction.y);
    }

    private void Idle()
    {
        _currentState = State.Idle;
        
        _player.NotInvulnerable();
        _movement.NotDash();
        
        _animator.SetBool("IsWalk", false);
    }

    private void Move(Vector2 direction)
    {
        if (direction.sqrMagnitude <= 0.1 && _currentState != State.Attack)
        {
            Idle();
            return;
        }
        
        _currentState = State.Move;
        _movement.Move(direction);
        _animator.SetBool("IsWalk", true);
    }

    private void Dash()
    {
        if (_currentState == State.Move && _player.Stamina >= _dashStamine)
        {
            _player.RemoveStamina(_dashStamine);
            _player.Invulnerable();

            _movement.Dash();
            _movement.Move(_direction);
        }
    }

    private void Attack()
    {
        if (_player.Stamina >= _attackStamine)
        {
            _animator.Play("Attack");
            
            _currentState = State.Attack;

            _player.RemoveStamina(_attackStamine);
            _attack.Attack();
        }
    }
    
    private enum State
    {
        Idle,
        Move, 
        Attack,
    }
}
