using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using QuestSystem;
using UnityEngine;
using UnityEngine.Serialization;

public class Player : MonoBehaviour, IQuestCompletable
{ 
    [SerializeField]  private float _maxHealth;
    [SerializeField] private float _maxStamina;
    
    [SerializeField] private Menu _menu;

    private float _health = 0;
    private float _stamina = 10;
    private float _money = 0;
    private float _delayStamminUpp = 1;

    private bool _invulnerable;
    
    private StamineState _stamineState;

    public float MaxHealth => _maxHealth;
    public float MaxStamina => _maxStamina;
    public float Stamina => _stamina;

    public event Action<float> ChangeMoney;
    public event Action<float, float> ChangeHealth;
    public event Action<float, float> ChangeStamina;
    public event Action<Npc> OnTouchNpc;
    public event Action<Marker> OnMarkedItemTouch;

    private void Start()
    {
        ResetState();
        
        ChangeHealth?.Invoke(_health, _maxHealth);
        ChangeMoney?.Invoke(_money);
        ChangeStamina?.Invoke(_stamina,_maxStamina);
        StaminRemoved();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.TryGetComponent(out Npc npc))
        {
            TouchNpc(npc);
        }
        
        if (other.gameObject.TryGetComponent(out Marker marker))
        {
            TouchQuestMarkedItem(marker);
        }
    }

    private void Update()
    {
        if(_stamineState == StamineState.StamineRemoved)
        {
            _delayStamminUpp -= Time.deltaTime;
            
            if (_delayStamminUpp <= 0)
            {
                AddStamin();
            }
        }
    }

    public void Invulnerable()
    {
        _invulnerable = true;
    }
    
    public void NotInvulnerable()
    {
        _invulnerable = false;
    }
    
    public void ResetState()
    {
        _health = _maxHealth;
        _stamina = _maxStamina;
        
        ChangeHealth?.Invoke(_health, _maxHealth);
        ChangeStamina?.Invoke(_stamina,_maxStamina);
    }

    private void Die()
    {
        _menu.GameOver();
    }

    public void TakeDamage(float damage)
    {
        if(_invulnerable == true)
            return;

        RemoveValue(ref _health, damage);

        if (_health <= 0)
            Die();
        
        ChangeHealth?.Invoke(_health, _maxHealth);
    }

    public void AddHealth(float treatment)
    {
        AddValue(ref _health, treatment);

        if (_health >= _maxHealth)
            _health = _maxHealth;

        ChangeHealth?.Invoke(_health, _maxHealth);
    }

    public void AddMaxHealt(float maxvalue)
    {
        _maxHealth += maxvalue;
    }

    public void AddMoney(float reward)
    {
        AddValue(ref _money, reward);
        
        ChangeMoney?.Invoke(_money);
    }

    public void RemoveMoney(float price)
    {
        if (_money < price)
         return;
            
        RemoveValue(ref _money,price);

        if (_money <= 0)
            _money = 0;
        
        ChangeMoney?.Invoke(_money);
    }
    
    public void AddStamina(float value)
    {
        AddValue(ref _stamina, value);

            ChangeStamina?.Invoke(_stamina,_maxStamina);
    }

    public void RemoveStamina(float value)
    {
        RemoveValue(ref _stamina, value);

            if (_stamina <= 0)
                _stamina = 0;

            ChangeStamina?.Invoke(_stamina, _maxStamina);
            StaminRemoved();
    }

    private void RemoveValue( ref float valueA, float valueB)
    {
        valueA -= valueB;
    }
    
    private void AddValue( ref float valueA, float valueB)
    {
        valueA += valueB;
    }
    
    private void StaminRemoved()
    {
        if (_stamina >= _maxStamina)
        {
            Staminfull();
            return;
        }

        _stamineState = StamineState.StamineRemoved;
        _delayStamminUpp = 1;
    }

    private void AddStamin()
    {
        _stamineState = StamineState.AddStamine;
        AddStamina(2);
        StaminRemoved();
    }

    private void Staminfull()
    {
        _stamineState = StamineState.Staminfull;
        _delayStamminUpp = 0;
    }
    
    private enum StamineState
    {
        StamineRemoved,
        AddStamine,
        Staminfull,
    }

    
    public void TouchNpc(Npc npc)
    {
        OnTouchNpc?.Invoke(npc);
    }

    public void TouchQuestMarkedItem(Marker markedItem)
    {
        OnMarkedItemTouch?.Invoke(markedItem);
    }
}
