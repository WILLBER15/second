using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _speed;

    private Vector3 _direction;

    private bool _dashed;

    public void Move(Vector2 direction)
    {
        _direction = new Vector3(direction.x, direction.y);
        float scaleMoveSpeed = _speed * Time.deltaTime;
        transform.position += _direction * scaleMoveSpeed;
    }

    public void Dash()
    {
        if (_dashed == false)
        {
            _speed++;
            _dashed = true;
        }
    }

    public void NotDash()
    {
        if (_dashed == true)
        {
            _speed--;
            _dashed = false;
        }
    }

    private void Rotate(Vector2 direction)
    {
        float targetAngel = Mathf.Atan2(-direction.x, direction.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, targetAngel);
    }
}
