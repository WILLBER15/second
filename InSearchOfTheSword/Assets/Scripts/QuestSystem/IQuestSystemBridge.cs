using UnityEngine;

namespace QuestSystem
{
    public interface IQuestSystemBridge
    {
        public void ReceiveSelectToProvider(bool answer);
    }
}
