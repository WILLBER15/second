﻿using UnityEngine;

namespace QuestSystem.ScriptableObjects
{
    public abstract class AAgreement : MonoBehaviour
    {
        [SerializeField]
        protected  Marker _marker;

        private void Start()
        {
            Debug.LogError("It's not gameplay object!!!");
            Destroy(gameObject);
        }

        protected virtual bool Condition(Marker[] markers)
        {
            return false;
        }

        public Status CompleteCondition(Status status, Marker[] markers)
        {
            if (Condition(markers) && status != Status.Done)
            {
                Reward();
                return Status.Done;
            }

            return status;
        }

        protected virtual void Reward(){}
    }
}