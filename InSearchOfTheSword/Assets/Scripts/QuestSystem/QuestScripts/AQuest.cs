﻿using Common;
using UnityEngine;

namespace QuestSystem.ScriptableObjects
{
    public abstract class AQuest : ScriptableObject
    {
        [SerializeField] protected int _localId;
        [SerializeField] protected string _questName;
        [SerializeField] [Multiline] protected string _description;
        [SerializeField] [Multiline] protected string _question;
        [SerializeField] [Multiline] protected string _congratulation;

        [SerializeField]
        protected AAgreement _agreement;

        [SerializeField]
        protected Npc _npc;

        [SerializeField] protected Status _status = Status.Waiting;


        public int LocalId => _localId;
        public string Description => _description;
        public string Question => _question;
        public Npc Npc => _npc;
        public string QuestName => _questName;

        public string Congratulation => _congratulation;


        public Status GetStatus()
        {
            return _status;
        }
        public void UpdateStatus(Status status)
        {
            _status = status;
        }
        
        public void CheckCompleteQuest(Marker[] markers)
        {
            var currentStatus = GetStatus();
            var newStatus = _agreement.CompleteCondition(currentStatus, markers);
            UpdateStatus(newStatus);
        }
    }
}