using QuestSystem;
using UnityEngine;

public class CaretakerToothMarker : Marker
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.TryGetComponent(out Player player))
        {
            Destroy(gameObject);
        }
    }
}
