
using UnityEngine;

namespace QuestSystem.ScriptableObjects.Agreements.Scripts
{
    public class KillMonsterAgreement : AAgreement
    {
        protected override bool Condition(Marker[] markers)
        {
            foreach (var marker in markers)
            {
                if (marker.GetType() == typeof(CaretakerToothMarker))
                    return true;
            }
            return true;
        }

        protected override void Reward()
        {
            base.Reward();
        }
    }
}
