using System.Collections;
using System.Collections.Generic;
using QuestSystem;
using QuestSystem.ScriptableObjects;
using UnityEngine;

public class FindFatherAgreement : AAgreement
{
    protected override bool Condition(Marker[] markers)
    {
        return base.Condition(markers);
    }

    protected override void Reward()
    {
        base.Reward();
    }
}
