using System.Linq;
using QuestSystem.QuestScripts.Agreements.Scripts.Markers;
using QuestSystem.ScriptableObjects;
using UnityEngine;

namespace QuestSystem.QuestScripts.Agreements.Scripts.Agreements
{
    public class HealthPotionAgreement : AAgreement
    {
        [SerializeField] private int _potionCount;
        public int Count => _potionCount;
        protected override bool Condition(Marker[] markers)
        {
            Debug.Log(markers.Length);
            var potionMarkers = markers.Where(marker => 
                marker.GetType() == typeof(HealthPotionMarker)).ToList();
            
            return potionMarkers.Count >= _potionCount;
        }

        protected override void Reward()
        {
            base.Reward();
        }
    }
}
