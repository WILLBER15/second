using QuestSystem.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace QuestSystem.QuestScripts
{
    public class QuestListItem : MonoBehaviour
    {
        [SerializeField] private Text _text;
        [SerializeField] private Color _activeColor;
        [SerializeField] private Color _doneColor;
        private Text _descriptionText;
        private Quest _quest;
        private void Awake()
        {
            var questLog = GetComponentInParent<QuestLog>();
            if (questLog)
            {
                var description = questLog.GetComponentInChildren<DescriptionText>();
                if (description)
                    _descriptionText = description.GetComponent<Text>();
            }
        }

        public void SetQuest(Quest quest)
        {
            _quest = quest;
            _text.text = quest.QuestName;
            _text.color = quest.GetStatus() == Status.Done ? _doneColor : _activeColor;    
        }

        public void ViewDescription()
        {
            var description = $"{_quest.Npc.NpcName}\n<b>Description: </b> \n{_quest.Description}" +
                              $"\n<b>Status:</b> {_quest.GetStatus()}";
            _descriptionText.text = description;
        }
        
    }
}
