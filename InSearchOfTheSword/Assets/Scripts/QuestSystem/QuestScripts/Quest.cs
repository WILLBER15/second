using UnityEngine;

namespace QuestSystem.ScriptableObjects
{
    [CreateAssetMenu(menuName = "QuestSystem/Quest", fileName = "New Quest", order = 1)]
    
    public class Quest : AQuest
    {
        
    }
}
