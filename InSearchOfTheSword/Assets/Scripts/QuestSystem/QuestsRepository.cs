﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using QuestSystem.ScriptableObjects;
using UnityEngine;

namespace QuestSystem
{
    public class QuestsRepository : MonoBehaviour
    {
        [SerializeField] private List<Quest> _quests;

        public void AddQuest (Type npc, Quest quest)
        {
            _quests.Add(quest);
        }

        public Quest[] GetQuests(Type npc)
        {
            var npcQuests = new List<Quest>();
            foreach (var quest in _quests)
            {
                if (quest.Npc.GetType() == npc)
                {
                    if (quest.GetStatus() != Status.Active &&
                        quest.GetStatus() != Status.Done)
                        npcQuests.Add(quest);
                }                
            }

            return npcQuests.ToArray();
        }

        public Quest GetQuestFromNpc(Npc npc)
        {
            foreach (var quest in _quests)
            {
                if(quest.Npc.GetType() == npc.GetType())
                    if (quest.GetStatus() == Status.Active)
                            return quest;
                    
            }

            return null;
        }

        public Quest[] CollectQuestsWithStatus(Status status)
        {
            return _quests.Where(quest => quest.GetStatus() == status).ToArray();
        }
    }
}