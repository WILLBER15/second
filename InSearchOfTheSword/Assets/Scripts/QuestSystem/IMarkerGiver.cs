﻿namespace QuestSystem
{
    public interface IMarkerGiver
    {
        public Marker Marker { get; }
    }
    
}