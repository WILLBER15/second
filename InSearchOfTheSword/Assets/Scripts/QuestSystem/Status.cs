﻿using System;

namespace QuestSystem
{
    public enum Status
    {
        None,
        Waiting,
        Active,
        Done
    }
}