using System.Collections.Generic;
using QuestSystem.QuestScripts;
using QuestSystem.QuestSystemObjects;
using QuestSystem.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace QuestSystem
{
    public class QuestLog : MonoBehaviour
    {
        [SerializeField] private QuestSystemCore _questSystem;
        [SerializeField] private QuestArea _questArea;
        [SerializeField] private QuestListItem _listItem;
        [SerializeField] private QuestCapacity _capacity;
        
        private List<Quest> _quests;
        private QuestListItem[] _list;
        private Text _text;

        private void Awake()
        {
            _quests = new List<Quest>(10);
            var capacityText = _capacity.GetComponentInChildren<CapacityText>();
            if (capacityText)
                _text = capacityText.GetComponent<Text>();
        }

        public void Close()
        {
            gameObject.SetActive(false);
            if(Cursor.visible) Cursor.visible = false;
        }

        private void OnEnable()
        {
            if (_questSystem != null)
            {
                _quests.Clear();
                
                var activeQuests = _questSystem.GetQuests(Status.Active);
                var doneQuests = _questSystem.GetQuests(Status.Done);

                foreach (var quest in activeQuests)
                {
                    _quests.Add(quest);
                }

                foreach (var quest in doneQuests)
                {
                    _quests.Add(quest);
                }
                
                ViewCapacity(doneQuests.Length, _quests.Count);
            }
            
            CollectQuestItems();
            for (var i = 0; i < _quests.Count; i++)
            {
                if(i >= _list.Length)
                {
                    var item = CreateQuestItem();
                    item.SetQuest(_quests[i]);
                    item.gameObject.name = $"QuestListItem({i})";
                    continue;
                }
                _list[i].SetQuest(_quests[i]);
            }

            Time.timeScale = 0;
        }

        private void CollectQuestItems()
        {
            _list = GetComponentsInChildren<QuestListItem>();
        }

        private QuestListItem CreateQuestItem()
        {
            return Instantiate(_listItem, _questArea.transform);
        }

        private void ViewCapacity(int doneCount, int questsAmount)
        {
            _text.text = $"{doneCount}/{questsAmount}";
        }

        private void OnDisable()
        {
            Time.timeScale = 1;
        }
    }
}
