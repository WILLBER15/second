﻿using UnityEngine;

namespace QuestSystem
{
    public abstract class Marker : MonoBehaviour
    {
        public string Name { get; private set; }
    }
}