using UnityEngine;
using UnityEngine.UI;

public class DescriptionText : MonoBehaviour
{
    private Text _text;
    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        _text.text = "";
    }
}
