using System;

namespace QuestSystem
{
    public class QuestSystemBridge : IQuestSystemBridge
    {
        private QuestSystemProvider _provider;
        public QuestSystemBridge( QuestSystemProvider provider)
        {
            _provider = provider;
        }
        
        public void ReceiveSelectToProvider(bool answer)
        {
            _provider.OnSelectQuestResponse(answer);
        }

    }
}
