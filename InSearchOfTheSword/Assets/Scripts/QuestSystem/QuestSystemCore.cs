using QuestSystem.ScriptableObjects;
using UnityEngine;

namespace QuestSystem
{
    public class QuestSystemCore : MonoBehaviour
    {
        [SerializeField] private QuestSystemProvider _provider;
        
        public Quest[] GetQuests(Status status)
        {
            return _provider.GetQuestsWithStatus(status);
        }
    }
}
