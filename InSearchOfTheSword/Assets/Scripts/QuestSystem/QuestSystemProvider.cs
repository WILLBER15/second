﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using QuestSystem.ScriptableObjects;
using UnityEngine;

namespace QuestSystem
{
    public class QuestSystemProvider : MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private QuestsRepository _questsRepository;
        
         private  MarkerRepository _markerRepository;
         private Quest[] _npcQuests;
         private Quest _npcCurrentQuest;
        public event Action<string, string> OnQuestRequest;
        public event Action<string, string> OnAboutQuestQuestion;
        public event Action<string, string> OnQuestComplete;

        private void Awake()
        {
            _markerRepository = new MarkerRepository();
            _npcQuests = new Quest[]{ };
        }
        public void OnEnable()
        {
            _player.OnTouchNpc += GetNpcQuests;
            _player.OnMarkedItemTouch += AddMarkerToRepository;
        }

        public void OnDisable()
        {
            _player.OnTouchNpc -= GetNpcQuests;
            _player.OnMarkedItemTouch -= AddMarkerToRepository;
        }

        
        private Quest GetActiveQuestFromNpc(Npc npc)
        {
            return _questsRepository.GetQuestFromNpc(npc);
        }

        private void GetNpcQuests(Npc npc)
        {
            var type = npc.GetType();
            var complete = false;
            if (!type.GetInterfaces().Contains(typeof(IQuestGiver))) return;
            
            _npcQuests = _questsRepository.GetQuests(npc.GetType()).OrderBy(quest => quest.LocalId).ToArray();

            _npcCurrentQuest = GetActiveQuestFromNpc(npc);

            if(_npcCurrentQuest != null)
                complete = CheckCompleteCondition(_npcCurrentQuest);
            
            switch (complete)
            {
                case true:
                    OnQuestComplete?.Invoke(_npcCurrentQuest.Congratulation, _npcCurrentQuest.Npc.NpcName);
                    return;
                case false when _npcCurrentQuest != null:
                    OnAboutQuestQuestion?.Invoke(_npcCurrentQuest.Question, _npcCurrentQuest.Npc.NpcName);
                    break;
                default:
                    SelectQuest(_npcQuests);
                    break;
            }
        }

        private void SelectQuest(IReadOnlyList<Quest> npcQuests)
        {
            if (npcQuests.Count > 0)
                OnQuestRequest?.Invoke(npcQuests[0].Description, npcQuests[0].Npc.NpcName);
        }

        public void OnSelectQuestResponse(bool response)
        {
            if (response && !_npcCurrentQuest)
                BringQuestToPlayer(_npcQuests.First());
        }

        private bool CheckCompleteCondition(AQuest npcQuest)
        {
            if (npcQuest != null)
            {
                var questsMarkers = _markerRepository.GetAllMarkers();
                npcQuest.CheckCompleteQuest(questsMarkers);
                if(npcQuest.GetStatus() == Status.Done)
                    return true;
            }

            return false;
        }
        
        private void BringQuestToPlayer(AQuest quest)
        {
            if(quest)
            {
                quest.UpdateStatus(Status.Active);
            }
            else Debug.Log("Quest not found");
            
        }
       
        private void AddMarkerToRepository(Marker marker)
        {
            _markerRepository.AddMarker(marker);
        }

        public Quest[] GetQuestsWithStatus(Status status)
        {
            return _questsRepository.CollectQuestsWithStatus(status);
        }
    }
}