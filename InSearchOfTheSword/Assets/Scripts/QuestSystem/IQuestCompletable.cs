﻿using System;
using Common;
using UnityEngine;

namespace QuestSystem
{
    public interface IQuestCompletable
    {
        public event Action<Npc> OnTouchNpc;
        public event Action<Marker> OnMarkedItemTouch;

        public void TouchNpc(Npc npc);
        public void TouchQuestMarkedItem(Marker marker);
    }
}