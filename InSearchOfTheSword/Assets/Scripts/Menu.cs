using System;
using System.Collections;
using System.Collections.Generic;
using IJunior.TypedScenes;
using QuestSystem;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject _gameOverPanel;
    [SerializeField] private GameObject _menuPanel;
    
    [SerializeField] private Player _player;
    
    [SerializeField] private Transform _startPosition;
    [SerializeField] private QuestLog _questLog;

    private PlayerInput _input;
    
    private void Awake()
    {
        _input = new PlayerInput();
        _input.Enable();

        _input.Menu.Exit.performed += ctx => OpenMenu();
        _input.QuestLog.OpenClose.performed += _ => OpenQuestLog();
    }

    private void OpenQuestLog()
    {
        if (_questLog != null)
        {
            _questLog.gameObject.SetActive(!_questLog.gameObject.activeInHierarchy);
            Cursor.visible = !Cursor.visible;
        }
    }

    public void CloseMenu()
    {
        _menuPanel.SetActive(false);
        PauseOff();
    }

    public void LoadScene(string nameScene)
    {
        Samething.Load(_player, nameScene);
    }

    public void GameOver()
    {
        _gameOverPanel.SetActive(true);
        
        PauseOn();
    }

    public void ReturnGame()
    {
        _gameOverPanel.SetActive(false);
        
        PauseOff();

        _player.transform.position = _startPosition.position;
        _player.ResetState();
    }
    
    public void PauseOn()
    {
        Time.timeScale = 0;
    }

    public void PauseOff()
    {
        Time.timeScale = 1;
    }

    public void CursorON()
    {
        Cursor.visible = true;
    }

    public void CursorOFF()
    {
        Cursor.visible = false;
    }

    private void OpenMenu()
    {
        _menuPanel.SetActive(true);
        PauseOn();
    }
}
