﻿using TMPro;
using UnityEngine;

namespace Common
{
    public abstract class Npc : MonoBehaviour
    {
        [SerializeField] private string _npcName;
        [SerializeField] protected TMP_Text _text;
        public string NpcName => _npcName;
    }
}