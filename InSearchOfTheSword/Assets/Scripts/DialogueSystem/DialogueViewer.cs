using DialogueSystem;
using QuestSystem;
using TMPro;
using UnityEngine;

public class DialogueViewer : MonoBehaviour
{
    [SerializeField] private TMP_Text _phaseText;
    
    [SerializeField] private TMP_Text _nameText;

    [SerializeField] private Menu _menu;
    [SerializeField] private QuestSystemProvider _provider;
    [SerializeField] private Dialog _dialog;
    [SerializeField] private ButtonsPanel _buttonsPanel;
    
    private Phrase[] _phases;
    private char[] phase;

    private int _currentPhase = 0;
    private IQuestSystemBridge _bridge;


    private void Awake()
    {
        _phases = gameObject.GetComponents<Phrase>();
        
    }

    private void Start()
    {
        if(_provider)
            _bridge = new QuestSystemBridge(_provider);
    }

    private void OnEnable()
    {
        if(_provider)
        {
            _provider.OnQuestRequest += ViewPhraseQuestResponse;
            _provider.OnAboutQuestQuestion += ViewPhraseQuestResponse;
            _provider.OnQuestComplete += ViewPhraseQuestResponse;
        }    
    }

    private void OnDisable()
    {
        if(_provider)
        {
            _provider.OnQuestRequest -= ViewPhraseQuestResponse;
            _provider.OnAboutQuestQuestion -= ViewPhraseQuestResponse;
            _provider.OnQuestComplete -= ViewPhraseQuestResponse;
        }
    }

    public void VieweCurrentPhase()
    {
        if (_currentPhase == _phases.Length)
        {
            _currentPhase = 0;
          Close();
          return;
        }
        
        Open();

        _phaseText.text = _phases[_currentPhase].Text;
        _nameText.text = _phases[_currentPhase].Name;

        _currentPhase++;
    }

    private void Open()
    {
        _dialog.gameObject.SetActive(true);
        _menu.PauseOn();
    }

    private void Close()
    {
        _dialog.gameObject.SetActive(false);
        _menu.PauseOff();
    }

    private void OpenButtonsPanel()
    {
        if(_buttonsPanel)
            _buttonsPanel.gameObject.SetActive(true);
    }
    private void CloseButtonsPanel()
    {
        if(_buttonsPanel)
            _buttonsPanel.gameObject.SetActive(false);
    }

    private void ViewPhraseQuestResponse(string description, string npcName)
    {
        _phaseText.text = description;
        _nameText.text = npcName;
        OpenButtonsPanel();
        Open();
    }

    public void OnAgree()
    {
        _bridge.ReceiveSelectToProvider(true);
        CloseButtonsPanel();
        Close();
    }

    public void OnCancel()
    {
        _bridge.ReceiveSelectToProvider(false);
        CloseButtonsPanel();
        Close();
    }
    
}
