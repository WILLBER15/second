using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Phrase : MonoBehaviour
{
    [SerializeField] private string _name; 
    [SerializeField] private string text;

    public string Name => _name;
    public string Text => text;
}
