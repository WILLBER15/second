using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroing : MonoBehaviour
{
   
     private void OnTriggerEnter2D(Collider2D other)
     {
        if (other.TryGetComponent(out Player player))
        {
            Destroy(gameObject);
        }
     }
    
}
