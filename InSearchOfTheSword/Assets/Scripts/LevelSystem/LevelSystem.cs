using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelSystem : MonoBehaviour
{
    protected int _currentLevel = 1;

    protected float _currentExperiance;
    protected float _levelUpExperiance = 15;
    
    public event Action<int, float, float> LevelUped;

    private void Start()
    {
        LevelUped?.Invoke(_currentLevel, _currentExperiance, _levelUpExperiance);
    }

    public void AddExperiance(float experiance)
    {
        _currentExperiance += experiance;
        
        LevelUped?.Invoke(_currentLevel, _currentExperiance, _levelUpExperiance);

        if (_currentExperiance >= _levelUpExperiance)
        {
            _currentLevel++;

            _currentExperiance = _currentExperiance - _levelUpExperiance;
            _levelUpExperiance *= 1.25f;
            
            LevelUp();
        }
    }

    private void LevelUp()
    {
        DamageUp();
        HealthUp();
        
        LevelUped?.Invoke(_currentLevel, _currentExperiance, _levelUpExperiance);
    }

    protected abstract void DamageUp();
    protected abstract void HealthUp();

}
