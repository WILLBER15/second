using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevels : LevelSystem
{
    [SerializeField] private PlayerAttack _attack;
    [SerializeField] private Player _player;

    protected override void DamageUp()
    {
        _attack.AddDamage(_currentLevel / 2);
    }

    protected override void HealthUp()
    {
        _player.AddMaxHealt(_currentLevel);
    }
}
