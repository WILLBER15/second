using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLevels : LevelSystem
{
    [SerializeField] private int _level;
    
    [SerializeField] private AttackState _attack;
    [SerializeField] private Enemy.Enemy _enemy;

    private void Awake()
    {
        _currentLevel = _level;
        DamageUp();
        HealthUp();
    }

    protected override void DamageUp()
    {
        _attack.AddDamage(_currentLevel/2);
    }

    protected override void HealthUp()
    {
        _enemy.AddHealth(_currentLevel);
    }
}
