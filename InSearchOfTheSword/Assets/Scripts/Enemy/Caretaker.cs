using QuestSystem;
using Unity.Mathematics;
using UnityEngine;

namespace Enemy
{
    public class Caretaker : Enemy
    {
        [SerializeField] private Marker _marker;
       
        protected override void Die()
        {
            if (_marker == null)
            {
                base.Die();
                return;
            }
            
            var markerPosition = Position();
            Instantiate(_marker, markerPosition, quaternion.identity);
            base.Die();
        }

        private Vector3 Position()
        {
            var bounds = new Bounds();
            if (TryGetComponent(out SpriteRenderer spriteRenderer))
            {
                bounds = spriteRenderer.bounds;
            }
             
                
            var center = bounds.center;
            var height = bounds.size.y;

            return new Vector3(center.x, center.y + height, 0);
        }
    }
}
