using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAttackTransition : Transition
{
    [SerializeField] private AttackState _attack;

    private void Update()
    {
        if (_attack.Attacked == true)
        {
            _attack.AttackComplete();
            NeedTransit = true;
        }
    }
}
