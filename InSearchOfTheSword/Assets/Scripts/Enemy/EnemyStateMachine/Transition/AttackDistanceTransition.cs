using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDistanceTransition : Transition
{
    [SerializeField] private float _distance;

    public float Distance => _distance;
    
    private void Update()
    {
        if (Vector2.Distance(transform.position, Target.transform.position) < _distance)
        {
            NeedTransit = true;
        }
    }
}
