using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceTransition : Transition
{
    private void Update()
    {
        if (Vector2.Distance(transform.position, Target.transform.position) < 5)
        {
            NeedTransit = true;
        }
    }
}
