using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotAttackDistanceTransition : Transition
{
    [SerializeField] private AttackDistanceTransition _attackDistance;

    private float _distance;

    private float _error = 0.5f;

    private void Start()
    {
        _distance = _attackDistance.Distance + _error;
    }

    private void Update()
    {
        if (Vector2.Distance(transform.position, Target.transform.position) > _distance)
        {
            NeedTransit = true;
        }
    }
}
