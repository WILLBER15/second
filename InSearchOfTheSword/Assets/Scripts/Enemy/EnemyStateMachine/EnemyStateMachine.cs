﻿using System.Collections;
using System.Collections.Generic;
using Common;
using UnityEngine;

[RequireComponent(typeof(Character))]
public class EnemyStateMachine : MonoBehaviour
{
    [SerializeField] private State _firstState;

    [SerializeField] private Player _target;

    [SerializeField] private Animator _animator;
    
    private State _currentState;

    public State Current => _currentState;

    private void Start()
    {
        Reset(_firstState);
    }

    private void Update()
    {
        if (_currentState == null)
            return;

        var nextState = _currentState.GetNextState();
        if (nextState != null)
            Transit(nextState);
    }

    private void Reset(State startState)
    {
        _currentState = startState;

        if (_currentState != null)
            _currentState.Enter(_target, _animator);
    }

    private void Transit(State nextState)
    {
        if (_currentState != null)
            _currentState.Exit();

        _currentState = nextState;

        if (_currentState != null)
            _currentState.Enter(_target, _animator);
    }
}