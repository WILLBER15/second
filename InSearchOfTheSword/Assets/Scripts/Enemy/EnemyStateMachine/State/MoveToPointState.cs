using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPointState : State
{
    [SerializeField] private Transform[] _points;

    [SerializeField] private float _speed;

    private float _horizontal;
    private float _vertical;

    private int _currentPoint;
    
    void Update()
    {
        _animator.Play("Move");
        
        Transform target = _points[_currentPoint];

        transform.position = Vector2.MoveTowards(transform.position, target.position, _speed * Time.deltaTime);

        if (transform.position == target.position)
        {
            _currentPoint++;


            if (_currentPoint >= _points.Length)
            {
                _currentPoint = 0;
                
                _animator.SetFloat("Horizontal", 1);
                _animator.SetFloat("Vertical", 1);
                return;
            }

            var rotation = transform.position - _points[_currentPoint].position;
            PlayAnimation(rotation);
        }    
    }
    
    private void PlayAnimation(Vector3 target)
    {
        if (target.x > 1)
            _horizontal = 1;
        else
            _horizontal = -1;

        if (target.y > 1)
            _vertical = 1;
        else
            _vertical = -1;

        _animator.SetFloat("Horizontal", _horizontal);
        _animator.SetFloat("Vertical", _vertical);
    }
}
