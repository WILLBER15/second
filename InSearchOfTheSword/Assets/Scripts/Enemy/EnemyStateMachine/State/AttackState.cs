using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State
{
    [SerializeField] private float _damage;

    [SerializeField] private float _delay = 0.2f;
    [SerializeField] private float _throwDistance = 0;

    private float _currentDelayValue;

    private bool _attacked;

    public bool Attacked => _attacked;

    private void OnEnable()
    {
        _currentDelayValue = _delay;
    }

    void Update()
    {
        _currentDelayValue -= Time.deltaTime;
        
        if (_currentDelayValue <= 0)
        {
            _animator.Play("Attack");
            
            Target.TakeDamage(_damage);

            var position = (Target.transform.position - transform.position) * _throwDistance;
            Target.transform.Translate(position);

            _attacked = true;
        }
    }

    public void AddDamage(float value)
    {
        _damage += value;
    }

    public void AttackComplete()
    {
        _attacked = false;
    }
}
