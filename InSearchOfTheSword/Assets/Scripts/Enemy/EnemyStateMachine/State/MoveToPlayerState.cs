using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveToPlayerState : State
{
    [SerializeField] private NavMeshAgent _navMesh;

    private float _horizontal;
    private float _vertical;

    private void Awake()
    {
        if(_navMesh)
        {
            _navMesh.updateRotation = false;
            _navMesh.updateUpAxis = false;
        }
    }

    private void Update()
    {
        if(_navMesh)
        {
            _animator.Play("Move");
            
            _navMesh.updateRotation = false;
            _navMesh.updateUpAxis = false;

            _navMesh.SetDestination(Target.transform.position);
            
            var rotation = transform.position - Target.transform.position;
            PlayAnimation(rotation);
        }
    }

    private void PlayAnimation(Vector3 target)
    {
        if (target.x > 1)
            _horizontal = 1;
        else
            _horizontal = -1;

        if (target.y > 1)
            _vertical = 1;
        else
            _vertical = -1;

        _animator.SetFloat("Horizontal", _horizontal);
        _animator.SetFloat("Vertical", _vertical);
    }
}
