using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LimitedSide : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out LimiterEnemyLocationTransition enemy))
        {
            enemy.EnemyNeedRemove();
        }
    }
}
