using Common;
using UnityEngine;

namespace Enemy
{
    public abstract class Enemy : Character
    {
        [SerializeField] private float _health;
        [SerializeField] private float _experianceValue;

        [SerializeField] private int _amountExperianceSphere;
        
        [SerializeField] private MoneyBonus _moneyBonus;
        [SerializeField] private ExperianceBonus _experianceBonus;

        private PlayerLevels _playerLevels;

    
        protected virtual void Die()
        {
            Destroy(gameObject);
            Instantiate(_moneyBonus,transform.position, Quaternion.identity);
            SpawnExperianceSphere();
        }
    
        public virtual void TakeDamage(float damage, PlayerLevels levels)
        {
            _health -= damage;

            _playerLevels = levels;

            if (_health <= 0)
                Die();
        }

        public virtual void AddHealth(float value)
        {
            _health += value;
        }

        private void SpawnExperianceSphere()
        {
            for (int i = 0; i < _amountExperianceSphere; i++)
            {
               var bonus =  Instantiate(_experianceBonus, RandomPosition(), Quaternion.identity);
               bonus.Init(_experianceValue/_amountExperianceSphere, _playerLevels);
            }
        }

        private Vector2 RandomPosition()
        {
            return new Vector2(transform.position.x + Random.Range(-1,2), transform.position.y + Random.Range(-1,2));
        }
    }
}
